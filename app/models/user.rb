class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  before_save { self.name.capitalize! }
  before_save { self.email.downcase! }

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
	
	validates :name, presence:true, length: { maximum: 50 }
	validates :description, presence:true, length: { maximum: 150 }

	has_one_attached :avatar
end
